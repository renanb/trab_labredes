#include <stdio.h>
#include <string.h>

#ifdef _WIN32
	#include <winsock2.h>
#else
    #include <stdlib.h>
    #include <unistd.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#define SOCKET	int
#endif


#define BUFFER_SIZE 1250
#define BUFFER_BITS_SIZE BUFFER_SIZE*8

int main(int argc, char **argv){
	 struct sockaddr_in peer;
	 SOCKET s;
	 int porta, peerlen, i, rate;
	 int sent;
	 char ip[16], buffer[BUFFER_SIZE]; // buffer with 10.000 bits
	 float time;
	 
#ifdef _WIN32
	 WSADATA wsaData;
  
	if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0) {
		printf("Erro no startup do socket\n");
		exit(1);
	}
#endif

	//Como usar: gt -i <IP_Destino> -p <porta> -r <transfer_rate_(kbits)>
	 if(argc < 7) {
		  printf("Utilizar:\n");
		  printf("gt -i <IP_Destino> -p <porta> -r <transfer_rate_(kbits)> \n");
		  exit(1);
	 }

	// Pega parametros
	strcpy(ip, argv[2]); 
	porta = atoi(argv[4]);
	rate = atoi(argv[6]);

	// Inicializa buffer
	for(i=0;i<BUFFER_SIZE;i++) {
		buffer[i] = (char) i*i-8*i+47;
	}

	// Cria o socket na familia AF_INET (Internet) e do tipo UDP (SOCK_DGRAM)
	if((s = socket(AF_INET, SOCK_DGRAM,0)) < 0) {
		printf("Falha na criacao do socket\n");
		exit(1);
 	}
	 
	// Cria a estrutura do socket
	peer.sin_family = AF_INET;
	peer.sin_port = htons(porta);
	peer.sin_addr.s_addr = inet_addr(ip); 
	peerlen = sizeof(peer);

	printf("UDP traffic generator\n");
	printf("IP: %s \n",ip); 
	printf("Porta: %d \n",porta); 
	printf("Rate (kbits): %d \n", rate); 


    time = ( (float) BUFFER_BITS_SIZE / (rate * 1000) ) * 1000000;
	printf("Time: %f \n",time); 
	
	// Envia pacotes
	while(1)
	{
		sent = sendto(s, buffer, sizeof(buffer), 0, (struct sockaddr *)&peer, peerlen);
		usleep(time);

	}
}
